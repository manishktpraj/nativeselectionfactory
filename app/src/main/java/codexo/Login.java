package codexo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import codexo.neonlive.SessionManager;
import codexo.neonlive.com.Constant;
import codexo.neonlive.com.MainActivity;
import codexo.neonlive.com.Otp;
import codexo.neonlive.com.R;
import codexo.neonlive.com.Splash;
import retrofit2.Call;
import retrofit2.Callback;

public class Login extends AppCompatActivity {
    Button login;
    AppCompatActivity activity;
    TextInputEditText mobile_number;
    private ProgressDialog loading;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_new);
        activity = Login.this;
        login =findViewById(R.id.login);
        mobile_number  = findViewById(R.id.mobile_number);
        loading = Constant.getProgressBar(activity);
        session = new SessionManager(getApplicationContext());
      /*  Intent i = new Intent(activity, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(i);*/
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String go_next = "yes";
                String username_string = mobile_number.getText().toString();
                if(username_string.length()<10)
                {
                    int duration = Toast.LENGTH_SHORT;
                    mobile_number.setError("Please Enter a Valid Mobile Number");
                    go_next ="no";
                }

                if(go_next.equals("yes")) {
                    doLogin(username_string,"");
                  /*  Intent i = new Intent(activity, Otp.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(i);*/
                }
            }
        });
    }




    private void doLogin(final String username_string, final String password_string) {


         if (!loading.isShowing()){
            loading.show();
        }
        Call<String> call = Constant.getUrl().doLogin(username_string,Constant.device_id,Constant.fcm_token);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                loading.dismiss();

                String response = response_string.body();

                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            Constant.skip="yes";
                            Constant.fragments="main";

                            Toast.makeText(activity, "Login successfully", Toast.LENGTH_SHORT).show();

                            String localJSONObject2 = jsonObject.getString("results");
                            ///    session.createLoginSession(localJSONObject2);
                            Intent i = new Intent(activity, Otp.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("id",localJSONObject2);
                            activity.startActivity(i);
                            finish();
                        } else {
                            new AlertDialog.Builder(activity)
                                    .setCancelable(false)
                                    .setMessage("Login failed! Please enter correct username or password.")
                                    .setNegativeButton("ok", null).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                doLogin(username_string, password_string);
            }
        });
    }
}