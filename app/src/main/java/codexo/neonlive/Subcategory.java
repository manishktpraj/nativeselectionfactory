package codexo.neonlive;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonlive.com.Constant;
import codexo.neonlive.com.MainActivity;
import codexo.neonlive.com.R;
import codexo.neonlive.com.ui.player.YoutubeExoCustomPlayer;
import codexo.neonlive.com.ui.youtube.YoutubeActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class Subcategory extends AppCompatActivity {
    AppCompatActivity activity;
    String cat_id,cat_name,student_id;
    private ProgressDialog loading;
    SessionManager session;
    JSONArray result_cat,results_videos;
    private static LinearLayoutManager mLayoutManager;
    RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        Toolbar toolbar = findViewById(R.id.toolbarr);
        activity = Subcategory.this;

        Constant.setToolbar(activity, toolbar);
        loading = Constant.getProgressBar(Subcategory.this);


        cat_id =getIntent().getStringExtra("cat_id");
        cat_name =getIntent().getStringExtra("cat_name");

        session = new SessionManager(getApplicationContext());
          student_id = session.getUserId();
        recycler_view = findViewById(R.id.recycler_view);
        TextView title = findViewById(R.id.title);
        title.setText(cat_name);
        getdashboard(student_id,cat_id);
    }

    private void getdashboard(final String student_id,final String cat_id) {

        if(!loading.isShowing())
        {
            loading.show();

        }
        Call<String> call = Constant.getUrl().getsubcategory(student_id,cat_id);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                String response = response_string.body();

                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            result_cat = jsonObject.getJSONArray("result_cat");
                           if(result_cat.length()>0)
                           {
                               loading.dismiss();
                               final Subcategory.CustomAdapterCat adapter = new Subcategory.CustomAdapterCat(activity);
                               mLayoutManager = new LinearLayoutManager(activity,
                                       LinearLayoutManager.VERTICAL,
                                       false);
                               recycler_view.setLayoutManager(mLayoutManager);
                               recycler_view.setHasFixedSize(true);
                               recycler_view.setAdapter(adapter);
                               loading.dismiss();
                           }else{
                               getvideos(student_id,cat_id);
                           }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getdashboard(student_id,cat_id);
            }
        });

    }
    private void getvideos(final String student_id,final String cat_id) {

        if(!loading.isShowing())
        {
            loading.show();

        }
        Call<String> call = Constant.getUrl().getvideos(student_id,cat_id);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {
                loading.dismiss();

                String response = response_string.body();

                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            loading.dismiss();

                            results_videos = jsonObject.getJSONArray("results");
                            if(results_videos.length()>0)
                            {
                                final Subcategory.CustomAdapter adapter = new Subcategory.CustomAdapter(activity);
                                mLayoutManager = new LinearLayoutManager(activity,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                recycler_view.setLayoutManager(mLayoutManager);
                                recycler_view.setHasFixedSize(true);
                                recycler_view.setAdapter(adapter);
                            }else{

                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getdashboard(student_id,cat_id);
            }
        });

    }
    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_cat_list,
                    parent, false);
            Subcategory.CustomAdapterCat.MainListHolder listHolder =
                    new Subcategory.CustomAdapterCat.MainListHolder(itemView);
            return listHolder;

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final Subcategory.CustomAdapterCat.MainListHolder mainHolder =
                    (Subcategory.CustomAdapterCat.MainListHolder) holder;



            try {
                mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //  Intent i = new Intent(activity, YoutubeActivity.class);
                        Intent i = new Intent(activity, Subcategory.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        JSONObject childata = null;
                        String cat_name="",cat_id="";
                        try {
                            childata = result_cat.getJSONObject(position);
                            cat_name =childata.getString("bcat_name");
                            cat_id =childata.getString("bcat_id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        i.putExtra("cat_name",cat_name);
                        i.putExtra("cat_id",cat_id);
                        activity.startActivity(i);
                    }
                });
              mainHolder.title.setText( result_cat.getJSONObject(position).getString("bcat_name"));

                String imageUri = result_cat.getJSONObject(position).getString("bcat_image");
                Log.d(imageUri, "  onBindViewHolder: ");
                if (!imageUri.equals("")) {
               Constant.setImage(imageUri, mainHolder.video_image);
                }else{
                    Constant.setImage(Constant.NO_IMAGE_URL, mainHolder.video_image);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return result_cat.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top,title;
            ImageView video_image;
            CardView card;
            public MainListHolder(View view) {
                super(view);
                title = view.findViewById(R.id.title);
                video_image = view.findViewById(R.id.video_image_);
                card = view.findViewById(R.id.card);

            }
        }
    }


    public class CustomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_video_list, parent, false);
            Subcategory.CustomAdapter.MainListHolder listHolder = new Subcategory.CustomAdapter.MainListHolder(itemView);
            return listHolder;

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final Subcategory.CustomAdapter.MainListHolder mainHolder = (Subcategory.CustomAdapter.MainListHolder) holder;



            try {
                mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                         Intent i = new Intent(activity, YoutubeActivity.class);
                      //  Intent i = new Intent(activity, YoutubeExoCustomPlayer.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        JSONObject childata = null;
                        try {
                            childata = results_videos.getJSONObject(position);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        i.putExtra("videodetail",childata+"");
                        try {
                            i.putExtra("title",childata.getString("vid_title"));
                            i.putExtra("id",childata.getString("vid_id"));
                            i.putExtra("video_id",childata.getString("vid_url"));
                            i.putExtra("dis",childata.getString("vid_desc"));
                            i.putExtra("image",childata.getString("vid_image"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        activity.startActivity(i);
                    }
                });
                mainHolder.title.setText( results_videos.getJSONObject(position).getString("vid_title"));

                String imageUri = results_videos.getJSONObject(position).getString("vid_image");
                Log.d(imageUri, "  onBindViewHolder: ");
                if (!imageUri.equals("")) {
                    Constant.setImage(imageUri, mainHolder.video_image);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return results_videos.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top,title;
            ImageView video_image;
            CardView card;
            public MainListHolder(View view) {
                super(view);
                title = view.findViewById(R.id.title);
                video_image = view.findViewById(R.id.video_image_);
                card = view.findViewById(R.id.card);

            }
        }
    }
}