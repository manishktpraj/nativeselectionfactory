package codexo.neonlive.com.ui.draft;

import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import codexo.neonlive.com.BuildConfig;
import codexo.neonlive.com.R;
import codexo.neonlive.com.databinding.DraftFragmentBinding;
import codexo.neonlive.com.ui.draft.adapter.DraftAdapter;
import codexo.neonlive.com.ui.draft.database.DatabaseRepository;
import codexo.neonlive.com.ui.draft.database.entities.DownloadFile;
import codexo.neonlive.com.ui.draft.model.DataItem;
import codexo.neonlive.com.ui.draft.navigator.DraftNavigator;
import codexo.neonlive.com.ui.draft.player.ExoPlayer;
import codexo.neonlive.com.ui.draft.viewmodel.DraftViewModel;

import static codexo.neonlive.com.ui.draft.viewmodel.DraftViewModel.getImageDirectory;
import static codexo.neonlive.com.ui.draft.viewmodel.DraftViewModel.getVideoDirectory;

public class DraftFragment extends Fragment implements DraftNavigator {
    private DraftAdapter adapter;
    private DraftFragmentBinding binding;
    private List<DataItem> getAllVideos = new ArrayList<>();
    private DraftViewModel viewModel;
    private DraftNavigator draftNavigator;
    private static DraftFragment instance;
    private DatabaseRepository repository;

    public static DraftFragment newInstance() {
        if(instance==null){
            return new DraftFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(requireContext()), R.layout.draft_fragment, container, false);
        repository=new DatabaseRepository(requireContext());
        draftNavigator=this;
        repository.getDownloadTable().observe(requireActivity(), new Observer<List<DownloadFile>>() {
            @Override
            public void onChanged(List<DownloadFile> tables) {
                if(tables!=null && tables.size()>0){
                    binding.emptyTxt.setVisibility(View.GONE);
                    adapter = new DraftAdapter(tables, requireContext(),repository, draftNavigator);
                    binding.draftVideos.setAdapter(adapter);
                }else{
                    binding.emptyTxt.setVisibility(View.VISIBLE);
                    binding.emptyTxt.setText(getString(R.string.no_video_found));
                }
            }
        });
        if(DraftViewModel.isStoragePermissionGranted(requireActivity())){
            binding.emptyTxt.setText(getString(R.string.no_video_found));
            binding.emptyTxt.setVisibility(View.GONE);
            WorkFlow();
        }else{
            binding.emptyTxt.setText(getString(R.string.storage_permission));
            binding.emptyTxt.setVisibility(View.VISIBLE);
            Toast.makeText(requireContext(), "Storage Permission Required", Toast.LENGTH_SHORT).show();
        }

        return binding.getRoot();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode==1){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireContext(), "Storage Permission Granted", Toast.LENGTH_SHORT).show();
                binding.emptyTxt.setVisibility(View.GONE);
                binding.emptyTxt.setText(getString(R.string.no_video_found));
                WorkFlow();

            }
            else {
                binding.emptyTxt.setText(getString(R.string.storage_permission));
                binding.emptyTxt.setVisibility(View.VISIBLE);
                Toast.makeText(requireContext(), "Storage Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void WorkFlow() {
  /*      draftNavigator = this;
        try{
            getAllVideos = createVideosForAdapter(requireContext());
            if (getAllVideos != null && getAllVideos.size() > 0) {
                binding.emptyTxt.setVisibility(View.GONE);
                adapter = new DraftAdapter(getAllVideos, requireContext(), draftNavigator);
                binding.draftVideos.setAdapter(adapter);
            } else {
                binding.emptyTxt.setText(getString(R.string.no_video_found));
                binding.emptyTxt.setVisibility(View.VISIBLE);
            }
        }catch (Exception e ){
            binding.emptyTxt.setText(getString(R.string.no_video_found));
            binding.emptyTxt.setVisibility(View.VISIBLE);
        }*/


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(DraftViewModel.class);
    }

    @Override
    public void onClickOnVideo(String videoUrl) {
        final File videoFile = new File(videoUrl);
       startActivity(new Intent(requireContext(), ExoPlayer.class).putExtra("offline",videoUrl));

        /*    Uri photoURI = FileProvider.getUriForFile(requireContext(), requireActivity().getApplicationContext().getPackageName() + ".provider", videoFile);
        *//*ShareCompat.IntentBuilder.from(getActivity())
                .setStream(photoURI)
                .setType("video/mp4")
                .setChooserTitle("Start Video...")
                .startChooser();*//*
        MediaScannerConnection.scanFile(getActivity(), new String[] { videoFile.getAbsolutePath() },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(
                                android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(
                                android.content.Intent.EXTRA_SUBJECT, videoFile.getName());
                        shareIntent.putExtra(
                                android.content.Intent.EXTRA_TITLE, videoFile.getName());
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        requireActivity().startActivity(Intent.createChooser(shareIntent, "Video "));

                    }
                });*/

    }
    public List<DataItem> createVideosForAdapter(Context context) {
        List<DataItem> mediaObjectList = new ArrayList<>();
        ArrayList<File> videos = new ArrayList<>();
        ArrayList<File> image = new ArrayList<>();
        videos = DraftViewModel.getListFiles(new File(getVideoDirectory(context)));
        image = DraftViewModel.getListFiles(new File(getImageDirectory(context)));
        Collections.sort(videos, new Comparator<File>() {
            @Override
            public int compare(File lhs, File rhs) {
                return lhs.getAbsolutePath().compareTo(rhs.getAbsolutePath());
            }
        });
        Collections.sort(image, new Comparator<File>() {
            @Override
            public int compare(File lhs, File rhs) {
                return lhs.getAbsolutePath().compareTo(rhs.getAbsolutePath());
            }
        });
        for (int i = videos.size() - 1; i >= 0; i--) {
            DataItem dataItem = new DataItem();
            dataItem.setId(i + "");
            int draft = i;
            dataItem.setFilename((videos.get(i).getAbsoluteFile().getName()));
            dataItem.setVideoUrl(videos.get(i).getAbsolutePath());
            dataItem.setThumbUrl(image.get(i).getAbsolutePath());
            dataItem.setTime(viewModel.getFormattedDate(context, videos.get(i).lastModified()));
            mediaObjectList.add(dataItem);

        }


        return mediaObjectList;
    }
}