package codexo.neonlive.com.ui.retrofitmodel.getDashboardData;

import com.google.gson.annotations.SerializedName;

public class ResultVideoItem{

	@SerializedName("vid_offline_url")
	private String vidOfflineUrl;

	@SerializedName("vid_image")
	private String vidImage;

	@SerializedName("vid_size")
	private String vidSize;

	@SerializedName("vid_type")
	private int vidType;

	@SerializedName("vid_category")
	private String vidCategory;

	@SerializedName("vid_title")
	private String vidTitle;

	@SerializedName("vid_publish_date")
	private Object vidPublishDate;

	@SerializedName("vid_study_material")
	private String vidStudyMaterial;

	@SerializedName("vid_medium")
	private String vidMedium;

	@SerializedName("vid_trans_code")
	private String vidTransCode;

	@SerializedName("vid_cdn_image")
	private String vidCdnImage;

	@SerializedName("vid_time")
	private String vidTime;

	@SerializedName("vid_desc")
	private String vidDesc;

	@SerializedName("vid_high")
	private String vidHigh;

	@SerializedName("vid_240")
	private String vid240;

	@SerializedName("vid_id")
	private int vidId;

	@SerializedName("vid_url")
	private String vidUrl;

	@SerializedName("vid_date")
	private String vidDate;

	@SerializedName("vid_offline240p_url")
	private String vidOffline240pUrl;

	@SerializedName("vid_low")
	private String vidLow;

	@SerializedName("vid_status")
	private boolean vidStatus;

	@SerializedName("video_slug")
	private String videoSlug;

	public void setVidOfflineUrl(String vidOfflineUrl){
		this.vidOfflineUrl = vidOfflineUrl;
	}

	public String getVidOfflineUrl(){
		return vidOfflineUrl;
	}

	public void setVidImage(String vidImage){
		this.vidImage = vidImage;
	}

	public String getVidImage(){
		return vidImage;
	}

	public void setVidSize(String vidSize){
		this.vidSize = vidSize;
	}

	public String getVidSize(){
		return vidSize;
	}

	public void setVidType(int vidType){
		this.vidType = vidType;
	}

	public int getVidType(){
		return vidType;
	}

	public void setVidCategory(String vidCategory){
		this.vidCategory = vidCategory;
	}

	public String getVidCategory(){
		return vidCategory;
	}

	public void setVidTitle(String vidTitle){
		this.vidTitle = vidTitle;
	}

	public String getVidTitle(){
		return vidTitle;
	}

	public void setVidPublishDate(Object vidPublishDate){
		this.vidPublishDate = vidPublishDate;
	}

	public Object getVidPublishDate(){
		return vidPublishDate;
	}

	public void setVidStudyMaterial(String vidStudyMaterial){
		this.vidStudyMaterial = vidStudyMaterial;
	}

	public String getVidStudyMaterial(){
		return vidStudyMaterial;
	}

	public void setVidMedium(String vidMedium){
		this.vidMedium = vidMedium;
	}

	public String getVidMedium(){
		return vidMedium;
	}

	public void setVidTransCode(String vidTransCode){
		this.vidTransCode = vidTransCode;
	}

	public String getVidTransCode(){
		return vidTransCode;
	}

	public void setVidCdnImage(String vidCdnImage){
		this.vidCdnImage = vidCdnImage;
	}

	public String getVidCdnImage(){
		return vidCdnImage;
	}

	public void setVidTime(String vidTime){
		this.vidTime = vidTime;
	}

	public String getVidTime(){
		return vidTime;
	}

	public void setVidDesc(String vidDesc){
		this.vidDesc = vidDesc;
	}

	public String getVidDesc(){
		return vidDesc;
	}

	public void setVidHigh(String vidHigh){
		this.vidHigh = vidHigh;
	}

	public String getVidHigh(){
		return vidHigh;
	}

	public void setVid240(String vid240){
		this.vid240 = vid240;
	}

	public String getVid240(){
		return vid240;
	}

	public void setVidId(int vidId){
		this.vidId = vidId;
	}

	public int getVidId(){
		return vidId;
	}

	public void setVidUrl(String vidUrl){
		this.vidUrl = vidUrl;
	}

	public String getVidUrl(){
		return vidUrl;
	}

	public void setVidDate(String vidDate){
		this.vidDate = vidDate;
	}

	public String getVidDate(){
		return vidDate;
	}

	public void setVidOffline240pUrl(String vidOffline240pUrl){
		this.vidOffline240pUrl = vidOffline240pUrl;
	}

	public String getVidOffline240pUrl(){
		return vidOffline240pUrl;
	}

	public void setVidLow(String vidLow){
		this.vidLow = vidLow;
	}

	public String getVidLow(){
		return vidLow;
	}

	public void setVidStatus(boolean vidStatus){
		this.vidStatus = vidStatus;
	}

	public boolean isVidStatus(){
		return vidStatus;
	}

	public void setVideoSlug(String videoSlug){
		this.videoSlug = videoSlug;
	}

	public String getVideoSlug(){
		return videoSlug;
	}

	@Override
 	public String toString(){
		return 
			"ResultVideoItem{" + 
			"vid_offline_url = '" + vidOfflineUrl + '\'' + 
			",vid_image = '" + vidImage + '\'' + 
			",vid_size = '" + vidSize + '\'' + 
			",vid_type = '" + vidType + '\'' + 
			",vid_category = '" + vidCategory + '\'' + 
			",vid_title = '" + vidTitle + '\'' + 
			",vid_publish_date = '" + vidPublishDate + '\'' + 
			",vid_study_material = '" + vidStudyMaterial + '\'' + 
			",vid_medium = '" + vidMedium + '\'' + 
			",vid_trans_code = '" + vidTransCode + '\'' + 
			",vid_cdn_image = '" + vidCdnImage + '\'' + 
			",vid_time = '" + vidTime + '\'' + 
			",vid_desc = '" + vidDesc + '\'' + 
			",vid_high = '" + vidHigh + '\'' + 
			",vid_240 = '" + vid240 + '\'' + 
			",vid_id = '" + vidId + '\'' + 
			",vid_url = '" + vidUrl + '\'' + 
			",vid_date = '" + vidDate + '\'' + 
			",vid_offline240p_url = '" + vidOffline240pUrl + '\'' + 
			",vid_low = '" + vidLow + '\'' + 
			",vid_status = '" + vidStatus + '\'' + 
			",video_slug = '" + videoSlug + '\'' + 
			"}";
		}
}