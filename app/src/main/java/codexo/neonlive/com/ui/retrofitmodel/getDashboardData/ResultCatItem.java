package codexo.neonlive.com.ui.retrofitmodel.getDashboardData;

import com.google.gson.annotations.SerializedName;

public class ResultCatItem{

	@SerializedName("bcat_slug")
	private String bcatSlug;

	@SerializedName("bcat_image")
	private String bcatImage;

	@SerializedName("bcat_parent")
	private int bcatParent;

	@SerializedName("bcat_meta")
	private String bcatMeta;

	@SerializedName("bcat_name")
	private String bcatName;

	@SerializedName("bcat_order")
	private int bcatOrder;

	@SerializedName("bcat_number")
	private String bcatNumber;

	@SerializedName("count")
	private int count;

	@SerializedName("bcat_id")
	private int bcatId;

	@SerializedName("bcat_description")
	private String bcatDescription;

	@SerializedName("bcat_status")
	private boolean bcatStatus;

	@SerializedName("bcat_old")
	private String bcatOld;

	public void setBcatSlug(String bcatSlug){
		this.bcatSlug = bcatSlug;
	}

	public String getBcatSlug(){
		return bcatSlug;
	}

	public void setBcatImage(String bcatImage){
		this.bcatImage = bcatImage;
	}

	public String getBcatImage(){
		return bcatImage;
	}

	public void setBcatParent(int bcatParent){
		this.bcatParent = bcatParent;
	}

	public int getBcatParent(){
		return bcatParent;
	}

	public void setBcatMeta(String bcatMeta){
		this.bcatMeta = bcatMeta;
	}

	public String getBcatMeta(){
		return bcatMeta;
	}

	public void setBcatName(String bcatName){
		this.bcatName = bcatName;
	}

	public String getBcatName(){
		return bcatName;
	}

	public void setBcatOrder(int bcatOrder){
		this.bcatOrder = bcatOrder;
	}

	public int getBcatOrder(){
		return bcatOrder;
	}

	public void setBcatNumber(String bcatNumber){
		this.bcatNumber = bcatNumber;
	}

	public String getBcatNumber(){
		return bcatNumber;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setBcatId(int bcatId){
		this.bcatId = bcatId;
	}

	public int getBcatId(){
		return bcatId;
	}

	public void setBcatDescription(String bcatDescription){
		this.bcatDescription = bcatDescription;
	}

	public String getBcatDescription(){
		return bcatDescription;
	}

	public void setBcatStatus(boolean bcatStatus){
		this.bcatStatus = bcatStatus;
	}

	public boolean isBcatStatus(){
		return bcatStatus;
	}

	public void setBcatOld(String bcatOld){
		this.bcatOld = bcatOld;
	}

	public String getBcatOld(){
		return bcatOld;
	}

	@Override
 	public String toString(){
		return 
			"ResultCatItem{" + 
			"bcat_slug = '" + bcatSlug + '\'' + 
			",bcat_image = '" + bcatImage + '\'' + 
			",bcat_parent = '" + bcatParent + '\'' + 
			",bcat_meta = '" + bcatMeta + '\'' + 
			",bcat_name = '" + bcatName + '\'' + 
			",bcat_order = '" + bcatOrder + '\'' + 
			",bcat_number = '" + bcatNumber + '\'' + 
			",count = '" + count + '\'' + 
			",bcat_id = '" + bcatId + '\'' + 
			",bcat_description = '" + bcatDescription + '\'' + 
			",bcat_status = '" + bcatStatus + '\'' + 
			",bcat_old = '" + bcatOld + '\'' + 
			"}";
		}
}