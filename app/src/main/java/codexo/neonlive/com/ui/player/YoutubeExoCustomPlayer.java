package codexo.neonlive.com.ui.player;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.StyledPlayerControlView;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonStreamParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;
import codexo.neonlive.SessionManager;
import codexo.neonlive.Subcategory;
import codexo.neonlive.com.Constant;
import codexo.neonlive.com.MainActivity;
import codexo.neonlive.com.Otp;
import codexo.neonlive.com.R;
import codexo.neonlive.com.databinding.ActivityCustomYoutubeBinding;
import codexo.neonlive.com.databinding.ActivityYoutubeExoCustomPlayerBinding;
import codexo.neonlive.com.ui.draft.customNotification.DownloadNotification;
import codexo.neonlive.com.ui.draft.database.DatabaseRepository;
import codexo.neonlive.com.ui.draft.database.entities.DownloadFile;
import codexo.neonlive.com.ui.draft.model.DownloadStatus;
import codexo.neonlive.com.ui.draft.service.YourService;
import codexo.neonlive.com.ui.draft.viewmodel.DraftViewModel;
import codexo.neonlive.com.ui.youtube.YouTubeHelper;
import retrofit2.Call;
import retrofit2.Callback;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.google.android.exoplayer2.util.Assertions.checkNotNull;

public class YoutubeExoCustomPlayer extends AppCompatActivity implements StyledPlayerControlView.VisibilityListener {
    private ActivityYoutubeExoCustomPlayerBinding binding;
    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private static final String STATE_RESUME_POSITION = "resume";
    private long mPlaybackPosition = 0;
    private int mCurrentWindow = 0;
    private DatabaseRepository repository;
    protected SimpleExoPlayer player;
    private boolean isShowingTrackSelectionDialog;
    private DataSource.Factory dataSourceFactory;
    private List<MediaItem> mediaItems;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private DebugTextViewHelper debugViewHelper;
    private TrackGroupArray lastSeenTrackGroupArray;
    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    private AdsLoader adsLoader;
    private Context context;
    private String YOUTUBE_VIDEO_ID = "";
    private String BASE_URL = "https://www.youtube.com";
    private String mYoutubeLink = "";
    private String url = "";
    private SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener;
    private PopupMenu menu;
    private String image;
    private ValueEventListener mSearchedLocationReferenceListener;
    DatabaseReference messagesRef;
    String titledata;
    TextView titlebox;
    LinearLayout sendchat;
    TextInputEditText typemessage;
    AppCompatActivity activity;
    SessionManager session;
    JSONArray arrayListchat = new JSONArray();
    String vid_id = "";
    private FirebaseDatabase mDatabase;
    private RelativeLayout chatlayout;
    private static LinearLayoutManager mLayoutManager;
    RecyclerView recycler_view;
    DownloadFile fileDownload;
    String offlineVideoUrl = "";
    JSONArray jsonarayquality = new JSONArray();

    Long currentpos = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_youtube_exo_custom_player);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            //  getWindow().getInsetsController().hide(WindowInsets.Type.statusBars());
        } else {

            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
            );

        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        vid_id = getIntent().getStringExtra("id");
        offlineVideoUrl = getIntent().getStringExtra("offline");
        repository = new DatabaseRepository(this);
        fileDownload = repository.getFile(vid_id);
        String MESSAGE_CHANNEL = "/neonlive/" + vid_id;
        Constant.logPrint(vid_id + "empty", "vid_id");
        mDatabase = FirebaseDatabase.getInstance();
        messagesRef = mDatabase.getReference().child(MESSAGE_CHANNEL);
        chatlayout = findViewById(R.id.chatlayout);
        session = new SessionManager(getApplicationContext());
        activity = YoutubeExoCustomPlayer.this;
        String userid = session.getUserId();
        recycler_view = findViewById(R.id.recycler_view);

        titledata = getIntent().getStringExtra("title");
        image = getIntent().getStringExtra("image");
        Log.d(titledata, "titledata");
        sendchat = findViewById(R.id.sendchat);
        typemessage = findViewById(R.id.typemessage);
        sendchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String go_next = "yes";
                String username_string = typemessage.getText().toString();
                if (username_string.equals("")) {
                    int duration = Toast.LENGTH_SHORT;
                    typemessage.setError("Please Enter Your Question");
                    go_next = "no";
                }

                if (go_next.equals("yes")) {
                    Long daedat = new Date().getTime();
                    JsonArray jsonObject = new JsonArray();
                    String userid = session.getUserId();
                    Map<String, String> formData = new HashMap<String, String>();
                    formData.put("user_id", userid);
                    formData.put("doubts_user_name", session.getUserName());
                    formData.put("doubts_user_email", session.getUserEmail());
                    formData.put("doubts_user_phone", session.getUserPhone());
                    formData.put("reply", "");
                    formData.put("date", daedat + "");
                    formData.put("comment", username_string);
                    messagesRef.child(daedat + "").setValue(formData);
                    typemessage.setText("");
                    Toast.makeText(activity, "Message Sent Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        });


        mSearchedLocationReferenceListener = messagesRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayListchat = new JSONArray();
                for (DataSnapshot locationSnapshot : dataSnapshot.getChildren()) {
                    String location = locationSnapshot.getValue().toString();
                    Log.d("Locations updated", "location: " + location);

                    try {
                        JSONObject objd = new JSONObject();
                        objd.put("user_id", locationSnapshot.child("user_id").getValue().toString());
                        objd.put("comment", locationSnapshot.child("comment").getValue().toString());
                        objd.put("date", locationSnapshot.child("date").getValue().toString());
                        objd.put("reply", locationSnapshot.child("reply").getValue().toString());
                        Log.d("Locations updated", "location: " + objd);


                        if (objd.getString("user_id").equals(session.getUserId())) {
                            arrayListchat.put(objd);

                        }

                        if (!recycler_view.equals(null)) {
                            final YoutubeExoCustomPlayer.CustomAdapterCat adapter = new YoutubeExoCustomPlayer.CustomAdapterCat(activity);
                            mLayoutManager = new LinearLayoutManager(activity,
                                    LinearLayoutManager.VERTICAL,
                                    false);
                            recycler_view.setLayoutManager(mLayoutManager);
                            recycler_view.setHasFixedSize(true);
                            recycler_view.setAdapter(adapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        dataSourceFactory = ExoUtilCustom.getDataSourceFactory(this);
        context = this;
        binding.playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
        binding.playerView.requestFocus();
        String urlMain = getIntent().getStringExtra("video_id");
        YOUTUBE_VIDEO_ID = new YouTubeHelper().extractVideoIdFromUrl(urlMain);
        mYoutubeLink = BASE_URL + "/watch?v=" + YOUTUBE_VIDEO_ID;
        extractYoutubeUrl();
        if (savedInstanceState != null) {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startWindow = savedInstanceState.getInt(KEY_WINDOW);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
            mCurrentWindow = savedInstanceState.getInt(KEY_WINDOW);
            mPlaybackPosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
        } else {
            DefaultTrackSelector.ParametersBuilder builder =
                    new DefaultTrackSelector.ParametersBuilder(/* context= */ this);
            trackSelectorParameters = builder.build();
            clearStartPosition();
        }


        binding.speed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, binding.speed);
                popupMenu.inflate(R.menu.options);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        // Toast message on menu item clicked
                        switch (menuItem.getItemId()) {
                            case R.id.qualityoption: {
                                if (player != null) {
                                    ///     player.setPlaybackParameters(new PlaybackParameters(0.5f));

                                    PopupMenu popupSpeed = new PopupMenu(context, binding.speed);
                                    popupSpeed.inflate(R.menu.quality);
                                    popupSpeed.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                        @Override
                                        public boolean onMenuItemClick(MenuItem menuItem) {
                                            // Toast message on menu item clicked
                                            switch (menuItem.getItemId()) {
                                                case R.id.p144_menu: {
                                                    if (player != null) {
                                                        ///   player.setPlaybackParameters(new PlaybackParameters(0.5f));

                                                        setquality(jsonarayquality.length() - 2);
                                                    }
                                                    break;
                                                }
                                                case R.id.p240_menu: {
                                                    if (player != null) {
                                                        ///   player.setPlaybackParameters(new PlaybackParameters(0.5f));
                                                        setquality(jsonarayquality.length() - 2);

                                                    }
                                                    break;
                                                }
                                                case R.id.p360_menu: {
                                                    if (player != null) {
                                                        ///     player.setPlaybackParameters(new PlaybackParameters(1f));
                                                        setquality(0);
                                                    }
                                                    break;
                                                }
                                                case R.id.p480_menu: {
                                                    if (player != null) {
                                                        setquality(0);

                                                        ////    player.setPlaybackParameters(new PlaybackParameters(1.5f));
                                                    }
                                                    break;
                                                }
                                                case R.id.p540_menu: {
                                                    if (player != null) {
                                                        setquality(2);

                                                        ////   player.setPlaybackParameters(new PlaybackParameters(2f));
                                                    }
                                                    break;
                                                }
                                                case R.id.p720_menu: {
                                                    if (player != null) {
                                                        setquality(2);

                                                        ////   player.setPlaybackParameters(new PlaybackParameters(2f));
                                                    }
                                                    break;
                                                }

                                            }
                                            return true;
                                        }
                                    });
                                    // Showing the popup menu
                                    popupSpeed.show();


                                }
                                break;
                            }
                            case R.id.speedoption: {
                                if (player != null) {

                                    PopupMenu popupSpeed = new PopupMenu(context, binding.speed);
                                    popupSpeed.inflate(R.menu.speed);
                                    popupSpeed.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                        @Override
                                        public boolean onMenuItemClick(MenuItem menuItem) {
                                            // Toast message on menu item clicked
                                            switch (menuItem.getItemId()) {
                                                case R.id.x05: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(0.5f));
                                                    }
                                                    break;
                                                }
                                                case R.id.normal: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(1f));
                                                    }
                                                    break;
                                                }
                                                case R.id.x125: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(1.25f));
                                                    }
                                                    break;
                                                }
                                                case R.id.x150: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(1.50f));
                                                    }
                                                    break;
                                                }
                                                case R.id.x175: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(1.75f));
                                                    }
                                                    break;
                                                }
                                                case R.id.x1: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(1.5f));
                                                    }
                                                    break;
                                                }
                                                case R.id.x2: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(2f));
                                                    }
                                                    break;
                                                }
                                                case R.id.x3: {
                                                    if (player != null) {
                                                        player.setPlaybackParameters(new PlaybackParameters(3f));
                                                    }
                                                    break;
                                                }
                                            }
                                            return true;
                                        }
                                    });
                                    // Showing the popup menu
                                    popupSpeed.show();


                                }
                                break;
                            }

                        }
                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();
            }
        });


        binding.exoMinimalFullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (getResources().getConfiguration().orientation) {
                    case Configuration.ORIENTATION_PORTRAIT: {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        break;
                    }
                    case Configuration.ORIENTATION_LANDSCAPE: {

                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                    }
                }
            }
        });


    }

    public void setquality(Integer i) {
        ///  player.release();
        try {
            if (jsonarayquality.length() > 0) {
                Integer d = i;
                url = jsonarayquality.get(d).toString();
                Log.d(url, "urlmanishgarg");
                if (!url.equals("")) {


                   /* MediaItem mediaItem = MediaItem.fromUri(url);
                    player.setMediaItem(mediaItem);
                    player.prepare();
                    player.seekTo(mCurrentWindow, mPlaybackPosition);*/

                    Long ppposition = player.getCurrentPosition();
                    int mCurrentWindows = mCurrentWindow;
                    releasePlayer();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            initializePlayer();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(ppposition + "", "demobalance");
                                    if (ppposition != 0L && player != null) {

                                        player.seekTo(mCurrentWindows, ppposition);
                                        player.getPlayWhenReady();
                                    }
                                }
                            }, 1000);
                        }
                    }, 1000);


                } else {


                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            onFullscreen(true);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            onFullscreen(false);

        }
    }

    public void onFullscreen(boolean fullscreen) {
    }

    private void extractYoutubeUrl() {
        @SuppressLint("StaticFieldLeak")
        YouTubeExtractor mExtractor = new YouTubeExtractor(this) {
            @Override
            protected void onExtractionComplete(SparseArray<YtFile> sparseArray, VideoMeta videoMeta) {
                if (sparseArray != null) {
                    //playVideo(sparseArray.get(22).getUrl());
                    binding.titleextra.setText(titledata);
                    Constant.logPrint("sparseArray", sparseArray + "sparseArray");
                    for (int i = 0; i < 5000; i++) {
                        if (sparseArray.get(i) != null) {
                            url = sparseArray.get(i).getUrl();
                            if (url != null) {
                                Constant.logPrint("url", url + "sparseArray");
                                if (jsonarayquality.length() <= 0) {
                                    initializePlayer();
                                    if (binding.playerView != null) {
                                        binding.playerView.onResume();
                                    }
                                }
                                jsonarayquality.put(url);
                            } else {
                                Toast.makeText(YoutubeExoCustomPlayer.this, "Video Url Not Supported", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonarayquality.length() >= 4) {
                                break;
                            }
                        }
                    }

                }
                Log.d(jsonarayquality + "", "jsonarayquality" + jsonarayquality.length());
            }
        };
        mExtractor.extract(mYoutubeLink, true, true);
        // playVideo(mYoutubeLink);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        releasePlayer();
        releaseAdsLoader();
        clearStartPosition();
        setIntent(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (player == null) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (player == null) {
            if (binding.playerView != null) {
                binding.playerView.onResume();
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mPlaybackPosition != 0L && player != null) {
                        player.seekTo(mCurrentWindow, mPlaybackPosition);
                        player.getPlayWhenReady();
                    }
                }
            }, 3000);

        }

    }

    @Override
    public void onPause() {


        super.onPause();
    }

    @Override
    public void onStop() {
        if (binding.playerView != null) {
            binding.playerView.onPause();
        }

        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        messagesRef.removeEventListener(mSearchedLocationReferenceListener);
        releaseAdsLoader();
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 0) {
            // Empty results are triggered if a permission is requested while another request was already
            // pending and can be safely ignored in this case.
            return;
        }
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initializePlayer();
        } else {
            showToast(R.string.storage_permission_denied);
            finish();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        // super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        updateStartPosition();
        outState.putInt(KEY_WINDOW, mCurrentWindow);
        outState.putLong(STATE_RESUME_POSITION, mPlaybackPosition);
        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
        outState.putInt(KEY_WINDOW, startWindow);
        outState.putLong(KEY_POSITION, startPosition);
        releasePlayer();
        super.onSaveInstanceState(outState);
    }


    // Activity input

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // See whether the player view wants to handle media or DPAD keys events.
        return binding.playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
    }


    @Override
    public void onVisibilityChange(int visibility) {
        binding.root.setVisibility(visibility);
    }

    @Override
    public void onBackPressed() {
        switch (getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT: {
                player.release();
                finish();
                break;
            }
            case Configuration.ORIENTATION_LANDSCAPE: {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            }
        }
    }

    protected boolean initializePlayer() {
        if (player == null) {
            RenderersFactory renderersFactory =
                    ExoUtilCustom.buildRenderersFactory(/* context= */ this, true);
            MediaSourceFactory mediaSourceFactory =
                    new DefaultMediaSourceFactory(dataSourceFactory)
                            .setAdsLoaderProvider(this::getAdsLoader)
                            .setAdViewProvider(binding.playerView);

            trackSelector = new DefaultTrackSelector(/* context= */ this);
            trackSelector.setParameters(trackSelectorParameters);
            lastSeenTrackGroupArray = null;
            player = new SimpleExoPlayer.Builder(/* context= */ this, renderersFactory)
                    .setMediaSourceFactory(mediaSourceFactory)
                    .setTrackSelector(trackSelector)
                    .build();
            player.addListener(new PlayerEventListener());
            player.addAnalyticsListener(new EventLogger(trackSelector));
            player.setAudioAttributes(AudioAttributes.DEFAULT, /* handleAudioFocus= */ true);
            player.setPlayWhenReady(startAutoPlay);
            binding.playerView.setPlayer(player);
        }
        binding.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.isDownload.setVisibility(View.VISIBLE);
                if (player != null) {

                    if (fileDownload == null) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(context, YourService.class);
                                // add infos for the service which file to download and where to store
                                intent.putExtra(YourService.FILENAME, titledata);
                                //  intent.putExtra(YourService.urlpath, url);
                                intent.putExtra(YourService.urlpath, offlineVideoUrl);
                                intent.putExtra(YourService.TYPE, "video");
                                intent.putExtra(YourService.VIDEO_ID, vid_id);
                                binding.isDownload.setVisibility(View.GONE);
                                startService(intent);
                            }
                        }, 2000);
                        Intent intent = new Intent(context, YourService.class);
                        // add infos for the service which file to download and where to store
                        intent.putExtra(YourService.FILENAME, titledata);
                        intent.putExtra(YourService.urlpath, image);
                        intent.putExtra(YourService.TYPE, "image");
                        intent.putExtra(YourService.VIDEO_ID, vid_id);
                        startService(intent);
                        Toast.makeText(context, "Downloading...", Toast.LENGTH_SHORT).show();
                    } else {
                        binding.isDownload.setVisibility(View.GONE);
                        Toast.makeText(context, "Already Downloaded", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    binding.isDownload.setVisibility(View.GONE);
                }
            }
        });
        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            ////////   player.seekTo(startWindow, startPosition);
        }

        //MediaItem mediaItem = MediaItem.fromUri("https://storage.googleapis.com/exoplayer-test-media-0/BigBuckBunny_320x180.mp4");
        MediaItem mediaItem = MediaItem.fromUri(url);
        player.setMediaItem(mediaItem);
        player.prepare();
        binding.selectTracksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackSelectionDialog trackSelectionDialog = TrackSelectionDialog.createForTrackSelector(
                        trackSelector, dismissedDialog -> isShowingTrackSelectionDialog = false);
                trackSelectionDialog.show(getSupportFragmentManager(), /* tag= */ null);
            }
        });
        updateButtonVisibility();
        return true;
    }


    private AdsLoader getAdsLoader(MediaItem.AdsConfiguration adsConfiguration) {
        // The ads loader is reused for multiple playbacks, so that ad playback can resume.
        if (adsLoader == null) {
            adsLoader = new ImaAdsLoader.Builder(/* context= */ this).build();
        }
        adsLoader.setPlayer(player);
        return adsLoader;
    }

    protected void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
            mediaItems = Collections.emptyList();
            trackSelector = null;
        }
        if (adsLoader != null) {
            adsLoader.setPlayer(null);
        }
    }

    private void releaseAdsLoader() {
        if (adsLoader != null) {
            adsLoader.release();
            adsLoader = null;
            binding.playerView.getOverlayFrameLayout().removeAllViews();
        }
    }

    private void updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }


    private void updateStartPosition() {
        if (player != null) {
            startAutoPlay = player.getPlayWhenReady();
            startWindow = player.getCurrentWindowIndex();
            mPlaybackPosition = player.getCurrentPosition();
            currentpos = player.getCurrentPosition();
            //startPosition = Math.max(0, player.getContentPosition());
        }
    }

    protected void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    // User controls

    private void updateButtonVisibility() {
        binding.selectTracksButton.setEnabled(player != null && TrackSelectionDialog.willHaveContent(trackSelector));
    }

    private void showControls() {
        //binding.debugTextView.setVisibility(View.VISIBLE);
    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    private class PlayerEventListener implements Player.EventListener {

        @Override
        public void onPlayWhenReadyChanged(boolean playWhenReady, int reason) {

        }


        @Override
        public void onPlaybackStateChanged(@Player.State int playbackState) {
            switch (playbackState) {
                case Player.STATE_ENDED: {
                    showControls();
                    break;
                }
                case Player.STATE_BUFFERING: {
                    showBuffring(true);
                    break;
                }
                case Player.STATE_READY: {
                    showBuffring(false);
                    break;
                }

            }

            updateButtonVisibility();
        }

        @Override
        public void onPlayerError(@NonNull ExoPlaybackException e) {
            if (isBehindLiveWindow(e)) {
                clearStartPosition();
                initializePlayer();
            } else {
                updateButtonVisibility();
                showControls();
            }
        }

        @Override
        @SuppressWarnings("ReferenceEquality")
        public void onTracksChanged(@NonNull TrackGroupArray trackGroups, @NonNull TrackSelectionArray trackSelections) {
            updateButtonVisibility();
            if (trackGroups != lastSeenTrackGroupArray) {
                MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        showToast(R.string.error_unsupported_video);
                    }
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        showToast(R.string.error_unsupported_audio);
                    }
                }
                lastSeenTrackGroupArray = trackGroups;
            }
        }
    }

    private void showBuffring(boolean b) {
    }

    private class PlayerErrorMessageProvider implements ErrorMessageProvider<ExoPlaybackException> {

        @Override
        @NonNull
        public Pair<Integer, String> getErrorMessage(@NonNull ExoPlaybackException e) {
            String errorString = getString(R.string.error_generic);
            if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                Exception cause = e.getRendererException();
                if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                    // Special case for decoder initialization failures.
                    MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                            (MediaCodecRenderer.DecoderInitializationException) cause;
                    if (decoderInitializationException.codecInfo == null) {
                        if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                            errorString = getString(R.string.error_querying_decoders);
                        } else if (decoderInitializationException.secureDecoderRequired) {
                            errorString =
                                    getString(
                                            R.string.error_no_secure_decoder, decoderInitializationException.mimeType);
                        } else {
                            errorString =
                                    getString(R.string.error_no_decoder, decoderInitializationException.mimeType);
                        }
                    } else {
                        errorString =
                                getString(
                                        R.string.error_instantiating_decoder,
                                        decoderInitializationException.codecInfo.name);
                    }
                }
            }
            return Pair.create(0, errorString);
        }
    }


    private void doLogin(final String username_string, final String password_string) {


        Call<String> call = Constant.getUrl().otpVerification(username_string, password_string);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {


                String response = response_string.body();

                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            //  Constant.skip="yes";
                            ///  Constant.fragments="main";


                        } else {
                            new AlertDialog.Builder(activity)
                                    .setCancelable(false)
                                    .setMessage("OTP does not match.")
                                    .setNegativeButton("ok", null).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                doLogin(username_string, password_string);
            }
        });
    }

    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatboxuser, parent, false);
            YoutubeExoCustomPlayer.CustomAdapterCat.MainListHolder listHolder = new YoutubeExoCustomPlayer.CustomAdapterCat.MainListHolder(itemView);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final YoutubeExoCustomPlayer.CustomAdapterCat.MainListHolder mainHolder = (YoutubeExoCustomPlayer.CustomAdapterCat.MainListHolder) holder;


            try {

                ///     mainHolder.title.setText( result_cat.getJSONObject(position).getString("bcat_name"));
                mainHolder.usercomment.setText(arrayListchat.getJSONObject(position).getString("comment"));
                String reply = arrayListchat.getJSONObject(position).getString("reply");
                if (!reply.equals("")) {
                    mainHolder.admincomment.setText(reply);
                } else {
                    mainHolder.admincomment.setVisibility(View.GONE);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayListchat.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top, title, admincomment, usercomment;
            ImageView video_image;
            CardView card;

            public MainListHolder(View view) {
                super(view);
                admincomment = view.findViewById(R.id.admincomment);
                usercomment = view.findViewById(R.id.usercomment);
            }
        }
    }

}