package codexo.neonlive.com.ui.retrofitmodel.getDashboardData;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DashBoardResponse{

	@SerializedName("result_video")
	private List<ResultVideoItem> resultVideo;

	@SerializedName("result_cat")
	private List<ResultCatItem> resultCat;

	@SerializedName("message")
	private String message;

	public void setResultVideo(List<ResultVideoItem> resultVideo){
		this.resultVideo = resultVideo;
	}

	public List<ResultVideoItem> getResultVideo(){
		return resultVideo;
	}

	public void setResultCat(List<ResultCatItem> resultCat){
		this.resultCat = resultCat;
	}

	public List<ResultCatItem> getResultCat(){
		return resultCat;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"DashBoardResponse{" + 
			"result_video = '" + resultVideo + '\'' + 
			",result_cat = '" + resultCat + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}