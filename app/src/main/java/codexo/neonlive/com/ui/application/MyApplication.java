package codexo.neonlive.com.ui.application;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;

import codexo.neonlive.com.ui.draft.service.YourService;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

// Setting timeout globally for the download network requests:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);
    }
}
