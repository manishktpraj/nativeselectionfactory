package codexo.neonlive.com.ui.draft.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import codexo.neonlive.com.ui.draft.database.entities.DownloadFile;

@Dao
public interface DaoFactory {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(DownloadFile table);

    @Query("Select * From DownloadFile ")
    LiveData<List<DownloadFile>> getAllDownloads();

    @Query("Select * From DownloadFile WHERE id=:videoId ")
    public DownloadFile getDownloadFile(String videoId);

    @Query("UPDATE DownloadFile SET downloadStatus=:downloadStatus , isDownloadComplete=:isDownloads WHERE downloadId = :downloadID")
    public void Update(String downloadStatus, boolean isDownloads, String downloadID);

    @Query("UPDATE DownloadFile SET currentBytes=:currentBytes , totalBytes=:totalBytes WHERE downloadId = :downloadID")
    public void Update(long currentBytes, long totalBytes, String downloadID);

    @Query("UPDATE DownloadFile SET videoUrlOffline=:videoUrl  WHERE downloadId = :downloadId")
    void Update(String videoUrl, String downloadId);

    @Query("UPDATE DownloadFile SET downloadId=:downloadId  WHERE id = :videoId")
    void UpdateDownload(String videoId, String downloadId);

    @Query("Delete from DownloadFile Where id = :videoId")
    void Delete (String videoId);

    @Query("Select * From DownloadFile WHERE id=:id ")
    LiveData<DownloadFile> getAllDownload(String id);
}
