package codexo.neonlive.com.ui.draft.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;

import codexo.neonlive.com.ui.draft.database.dao.DaoFactory;
import codexo.neonlive.com.ui.draft.database.entities.DownloadFile;

public class DatabaseRepository {
    private RoomDatabase database;
    private Context context;
    private DaoFactory dao;

    public DatabaseRepository(Context context) {
        this.context = context;
        dao = getDatabase(context).dao();
    }

    public RoomDatabase getDatabase(Context context) {
        if (database == null) {
            database = RoomDatabase.getInstance(context);
        }
        return database;
    }

    public LiveData<List<DownloadFile>>getDownloadTable() {
        return dao.getAllDownloads();
    }
    public LiveData<DownloadFile>getDownloadTable(String id) {
        return dao.getAllDownload(id);
    }
    public void Insert(DownloadFile file){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                dao.insert(file);
            }
        });
    }
    public void Update(long totalBytes,long currentBytes,String downloadId){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                dao.Update(currentBytes,totalBytes,downloadId);
            }
        });
    }
    public void UpdateDownloadId(String id,String downloadId){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                dao.UpdateDownload(id,downloadId);
            }
        });
    }
    public void Update(String status,boolean isDownload,String downloadId){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                dao.Update(status,isDownload,downloadId);
            }
        });
    }
    public void Update(String videoUrl,String downloadId){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                dao.Update(videoUrl,downloadId);
            }
        });
    }
    public void Delete(String videoID){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                dao.Delete(videoID);
            }
        });
    }
    public DownloadFile getFile(String id){
        try {
          return   new GetDownloadFile(dao,id).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class GetDownloadFile extends AsyncTask<Void, Void, DownloadFile> {
        private DaoFactory dao;
        private String videoId;

        public GetDownloadFile(DaoFactory dao, String videoId) {
            this.dao = dao;
            this.videoId = videoId;
        }

        @Override
        protected DownloadFile doInBackground(Void... voids) {
            return dao.getDownloadFile(videoId);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(DownloadFile inlineInspectionTable) {
            super.onPostExecute(inlineInspectionTable);
        }
    }

}
