package codexo.neonlive.com.ui.draft.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.downloader.PRDownloader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import codexo.neonlive.com.Constant;
import codexo.neonlive.com.MainActivity;
import codexo.neonlive.com.R;
import codexo.neonlive.com.databinding.ContentVideoListBinding;
import codexo.neonlive.com.databinding.DraftItemBinding;
import codexo.neonlive.com.ui.draft.database.DatabaseRepository;
import codexo.neonlive.com.ui.draft.database.entities.DownloadFile;
import codexo.neonlive.com.ui.draft.model.DataItem;
import codexo.neonlive.com.ui.draft.navigator.DraftNavigator;

public class DraftAdapter extends RecyclerView.Adapter<DraftAdapter.DraftViewholder> {
    private List<DownloadFile> getAllVideos = new ArrayList<>();
    private Context context;
    private DraftNavigator draftNavigator;
    private DatabaseRepository repository;
private  AlertDialog dialog;

    public DraftAdapter(List<DownloadFile> getAllVideos, Context context, DatabaseRepository repository, DraftNavigator draftNavigator) {
        this.getAllVideos = getAllVideos;
        this.context = context;
        this.draftNavigator = draftNavigator;
        this.repository=repository;
    }

    @NonNull
    @Override
    public DraftViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DraftItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.draft_item, parent, false);
        return new DraftViewholder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DraftViewholder holder, int position) {
        holder.OnBind(getAllVideos.get(position));

    }

    @Override
    public int getItemCount() {
        return getAllVideos.size();
    }

    class DraftViewholder extends RecyclerView.ViewHolder {
        private DraftItemBinding binding;

        public DraftViewholder(@NonNull DraftItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void OnBind(DownloadFile dataItem) {
            binding.title.setText(dataItem.getFileName());
            String image=dataItem.getThumbPath()+ File.separator+dataItem.getFileName()+".jpg";
            Glide.with(context).load(new File(image)).into(binding.videoImage);
            binding.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Are You Sure");
                    builder.setMessage("Do You Want to Delete this Video");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            repository.Delete(dataItem.getId());
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(dialog!=null){
                                dialog.dismiss();
                            }

                        }
                    });

                   dialog = builder.create();
                   dialog.setCancelable(false);
                    dialog.show();

                    Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    pbutton.setTextColor(Color.parseColor("#008000"));

                }
            });
            repository.getDownloadTable(dataItem.getId()).observe(((MainActivity) context), new Observer<DownloadFile>() {
                @Override
                public void onChanged(DownloadFile file) {
                    if(file!=null){
                        if(!file.isDownloadComplete()){
                            binding.videoImage.setAlpha(0.8f);
                            try {
                                long per = (file.getCurrentBytes() * 100) / file.getTotalBytes();
                                binding.progressBar.setProgress((int) per);
                            }catch (Exception e){

                            }

                        }else{
                            binding.videoImage.setAlpha(1f);
                            binding.progressBar.setProgress(100);
                        }
                    }
                }
            });

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(dataItem.isDownloadComplete()){
                        if(dataItem.getVideoUrlOffline()!=null){
                            //draftNavigator.onClickOnVideo(dataItem.getVideoUrlOffline());
                            draftNavigator.onClickOnVideo(dataItem.getVideoUrlOffline());
                        }else{
                            draftNavigator.onClickOnVideo(new File(dataItem.getFilepath()+ File.separator+dataItem.getFileName()+".mp4").getAbsolutePath());
                        }

                    }else{
                        Toast.makeText(context, "Error On File : Video is not Downloaded", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }
}
