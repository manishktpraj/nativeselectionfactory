package codexo.neonlive.com.ui.draft.player;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

import java.io.File;


class UniversalMediaSourceFactory
{

    @SuppressWarnings("FieldCanBeLocal")
    private final long MAX_CACHE_SIZE = 100 * 1024 * 1024;//bytes

    private final DataSource.Factory dataSourceFactory;

    private File cacheDir;

    private Cache downloadCache;

    private ExoDatabaseProvider databaseProvider;

    UniversalMediaSourceFactory(Context context, String userAgent)
    {
        //initialize cached files database provider
        databaseProvider = new ExoDatabaseProvider(context);

        //initialize download cache
        cacheDir = new File(context.getCacheDir(), "media");
        downloadCache = new SimpleCache(cacheDir, new LeastRecentlyUsedCacheEvictor(MAX_CACHE_SIZE), databaseProvider);

        //create http data source factory that allows http -> https redirects
        DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, null,
                DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);

        //initialize data source factory
        DefaultDataSourceFactory ddsf = new DefaultDataSourceFactory(context, httpDataSourceFactory);
        dataSourceFactory = new CacheDataSourceFactory(downloadCache, ddsf, CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR | CacheDataSource.FLAG_BLOCK_ON_CACHE);
    }

    MediaSource createMediaSource(Uri uri)
    {

        switch (Util.inferContentType(uri))
        {
            case C.TYPE_DASH:
            {
                //DASH stream
                 return new DashMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            }
            case C.TYPE_HLS:
            {
                //HLS stream
                 return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            }
            case C.TYPE_SS:
            {
                //SmoothStreaming stream
                return new SsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            }
            case C.TYPE_OTHER:
            {
                //Progressive stream
                return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            }
            default:
            {
                //invalid = type not supported
                 return null;
            }
        }
    }

    /**
     * Release resources allocated by the UniversalMediaSourceFactory
     */
    void release()
    {
        //log release + cached bytes
       //release cache
        if (downloadCache != null)
        {
            downloadCache.release();

        }

        //--clear cache &-- close database connection
        if (databaseProvider != null)
        {
            //SimpleCache.delete(cacheDir, databaseProvider);
            databaseProvider.close();
        }
    }
}