package codexo.neonlive.com.ui.draft.customNotification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import java.io.File;

public class DownloadNotification {

    public static NotificationManager mNotificationManager;
    static NotificationCompat.Builder builder;
    static Context context;
    static int NOTIFICATION_ID = 111;
    static String CHANNEL_NAME = "001";
    static DownloadNotification fileUploadNotification;
    static Notification notification;
    static NotificationChannel notificationChannel;


    public DownloadNotification(Context context) {
        mNotificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(context,CHANNEL_NAME);
    }

    public void UpdateNotification(Context context,String filename, int downloadId) {
        NOTIFICATION_ID=downloadId;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_NAME);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel("id",CHANNEL_NAME,NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("no sound");
            notificationChannel.setSound(null, null);
            notificationChannel.enableLights(false);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(false);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        builder.setContentTitle("Start Downloading...")
                .setContentText(filename)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setProgress(100, 0, false)
                .setAutoCancel(false);
        notification = builder.build();
        mNotificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public  void updateNotification(Context context,String percent, String fileName, String contentText) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_NAME);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel = new NotificationChannel("id",CHANNEL_NAME,NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setDescription("no sound");
                notificationChannel.setSound(null, null);
                notificationChannel.enableLights(false);
                notificationChannel.setLightColor(Color.BLUE);
                notificationChannel.enableVibration(true);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
            builder.setContentText(contentText)
                    .setContentTitle(fileName)
                    .setSmallIcon(android.R.drawable.stat_sys_download)
                    .setOngoing(true)
                    .setContentInfo(percent + "%")
                    .setProgress(100, Integer.parseInt(percent), false);
            notification = builder.build();
            mNotificationManager.notify(NOTIFICATION_ID, builder.build());
            if (Integer.parseInt(percent) == 100) {
                //deleteNotification();
              deleteNotification();
            }

        } catch (Exception e) {
            Log.e("Error...Notification.", e.getMessage() + ".....");
            e.printStackTrace();
        }
    }
    public void  DownloadComplete(String fileName, File videoFile, Context context,String downloadId){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_NAME);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel("id",CHANNEL_NAME,NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("no sound");
            notificationChannel.setSound(null, null);
            notificationChannel.enableLights(false);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        builder.setContentText("Dowloaded Successfully")
                .setContentTitle(fileName)
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setContentInfo(100 + "%")
                .setProgress(100, 100, false);
        notification = builder.build();
        mNotificationManager.notify(NOTIFICATION_ID, builder.build());

    }

    public  void failDownloadNotification(Context context,String downloadId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_NAME);
        Log.e("downloadsize", "failed notification...");

        if (builder != null) {
            /* if (percentage < 100) {*/
            builder.setContentText("Downloading Failed")
                    .setSmallIcon(android.R.drawable.stat_sys_download_done)
                    .setOngoing(false);
            mNotificationManager.notify(NOTIFICATION_ID, builder.build());

        } else {
            mNotificationManager.cancel(NOTIFICATION_ID);
        }
    }

    public static void deleteNotification() {
        mNotificationManager.cancel(NOTIFICATION_ID);
        builder = null;
    }
}
