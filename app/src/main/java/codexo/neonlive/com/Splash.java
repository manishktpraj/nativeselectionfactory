package codexo.neonlive.com;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nostra13.universalimageloader.core.ImageLoader;

import codexo.Login;
import codexo.neonlive.SessionManager;

public class Splash extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2000;
    Animation animFadein;
    AppCompatActivity activity;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        activity = Splash.this;

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        session = new SessionManager(getApplicationContext());

        if (Constant.imageLoader_ == null) {
            ImageLoader.getInstance().init(Constant.getImageLoaderConfig(getApplicationContext()));
            Constant.imageLoader_ = ImageLoader.getInstance();
        }

        Constant.printHashKey(activity);
        Constant.fullScreen(activity);


        ImageView loading = findViewById(R.id.logo);


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("", "getInstanceId failed", task.getException());
                            return;
                        }

                         Constant.fcm_token = task.getResult().getToken();
                        Constant.device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                        Log.d("demofcm", "onComplete: "+Constant.fcm_token);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                session.checkLogin();
                                finish();

                            }
                        }, SPLASH_TIME_OUT);
                    }

                });




    }
}