package codexo.neonlive.com;

import codexo.neonlive.com.ui.retrofitmodel.getDashboardData.DashBoardResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetResponse {

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.CHECK_REGISTER)
    Call<String> checkRegister(
            @Field("user_phone") String user_phone);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.OTP_VERIFY_FOR_REGISTER)
    Call<String> otpVerify(
            @Field("user_phone") String user_phone,
            @Field("user_otp") String user_otp);
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.SLIDER)
    Call<String> getSlider(
            @Field("slider_tyoe") String slider_tyoe);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.REGISTER)
    Call<String> doRegister(
            @Field("user_id") String user_id,
            @Field("user_mobile_number") String user_phone,
            @Field("user_first_name") String user_first_name,
            @Field("user_last_name") String user_last_name,
            @Field("user_email") String user_email,
            @Field("user_password") String user_password,
            @Field("referral_code") String referral_code,
            @Field("user_device_id") String device_id,
            @Field("user_tocken_id") String user_fcm_token);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.LOGIN)
    Call<String> doLogin(
            @Field("student_phone") String student_username,
            @Field("device_id") String device_id,
            @Field("fcm_token") String user_fcm_token);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.FAQ)
    Call<String> getFaq(
            @Field("fcm_token") String user_fcm_token);


    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.DASHBOARD)
    Call<String> getDashboardData(
            @Field("user_id") String user_id,
            @Field("userFirebaseToken") String userFirebaseToken,
            @Field("userDeviceId") String userDeviceId,
            @Field("userdeviceos") String userdeviceos,
              @Field("pageno") int  pageno);


    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.SUBCATEGORY)
    Call<String> getsubcategory(
            @Field("student_id") String student_id,
           @Field("bcat_id") String cat_id);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST("getvideo")
    Call<String> getvideos(
            @Field("user_id") String student_id,
            @Field("category") String cat_id);
    /*
    @Headers("Cache-Control: no-cache")
    @GET(Constant.VERSION_UPDATE)
    Call<String> versionUpdate();
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.MY_CART)
    Call<String> sendRequestForMyCart(
            @Field("user_id") String user_id);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.ADD_TO_CART)
    Call<String> sendRequestForAddToCart(
            @Field("product_id") String product_id,
            @Field("user_id") String user_id,
            @Field("product_qty") String product_qty);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.UPDATE_CART)
    Call<String> sendRequestUpdateCart(
            @Field("cart_id") String cart_id,
            @Field("cart_qty") String cart_qty,
            @Field("user_id") String user_id);
  
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.ADD_ADDRESS)
    Call<String> doaddnewaddress(
            @Field("ab_id") String ab_id,
            @Field("ab_name") String ab_name,
            @Field("ab_mobile_number") String ab_mobile_number,
            @Field("ab_pincode") String ab_pincode,
            @Field("ab_state") String ab_state,
            @Field("ab_address") String ab_address,
            @Field("ab_locallity") String ab_locallity,
            @Field("ab_type") String ab_type,
            @Field("ab_user_id") String user_id);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.ADDRESS_BOOK)
    Call<String> getAddress(
            @Field("user_id") String user_id);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.REMOVE_ADDRESS)
    Call<String> removeAddress(
            @Field("is_default") String is_default,
            @Field("ab_id") String ab_id,
            @Field("user_id") String user_id);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.DEFAULT_ADDRESS)
    Call<String> setDefaultAddress(
            @Field("ab_id") String ab_id,
            @Field("user_id") String user_id);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.CHANGE_PASSWORD)
    Call<String> changePassword(
            @Field("student_id") String student_id,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password);
   
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.USER_SHARE)
    Call<String> shareEarn(
            @Field("student_id") String student_id);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.FACEBOOK)
    Call<String> sendRequestForFacebookLogin(
            @Field("fbid") String fbid,
            @Field("facebooklogin") String facebooklogin,
            @Field("deviceid") String deviceid,
            @Field("fcmtocken") String fcmtocken,
            @Field("student_device") String student_device,
            @Field("student_device_type") String student_device_type,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("password") String password,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("imageurl") String imageurl,
            @Field("student_referral_code") String student_referral_code);
   
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.GOOGLE)
    Call<String> sendRequestForGoogleLogin(
            @Field("fbid") String fbid,
            @Field("facebooklogin") String facebooklogin,
            @Field("deviceid") String deviceid,
            @Field("fcmtocken") String fcmtocken,
            @Field("student_device") String student_device,
            @Field("student_device_type") String student_device_type,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("password") String password,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("imageurl") String imageurl,
            @Field("student_referral_code") String student_referral_code);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.FACEBOOK)
    Call<String> sendRequestForFacebook(
            @Field("fbid") String fbid,
            @Field("facebooklogin") String facebooklogin,
            @Field("deviceid") String deviceid,
            @Field("fcmtocken") String fcmtocken,
            @Field("student_device") String student_device,
            @Field("student_device_type") String student_device_type);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.GOOGLE)
    Call<String> sendRequestForGoogle(
            @Field("fbid") String fbid,
            @Field("facebooklogin") String facebooklogin,
            @Field("deviceid") String deviceid,
            @Field("fcmtocken") String fcmtocken,
            @Field("student_device") String student_device,
            @Field("student_device_type") String student_device_type);
   
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.FORGOT_PASSWORD)
    Call<String> forgotPassword(
            @Field("student_username") String student_username);
     
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.STUDENT_PROFILE)
    Call<String> sendRequestForUserData(
            @Field("student_id") String student_id);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.LOGIN)
    Call<String> sendRequestForLogin(
            @Field("student_username") String student_username,
            @Field("student_password") String student_password,
            @Field("deviceid") String deviceid,
            @Field("fcmtocken") String fcmtocken,
            @Field("student_device") String student_device,
            @Field("student_device_type") String student_device_type);
    
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.RE_SEND_OTP)
    Call<String> resendOtp(
            @Field("student_id") String student_id,
            @Field("deviceid") String deviceid,
            @Field("fcmtocken") String fcmtocken,
            @Field("student_device") String student_device,
            @Field("student_device_type") String student_device_type);
      
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.REGISTER_OTP_VERIFY)
    Call<String> otpVerify(
            @Field("student_id") String student_id,
            @Field("student_otp") String student_otp);
    */
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.OFFERCE)
    Call<String> getOffers(
            @Field("user_id") String user_id);
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.DATESLOT)
    Call<String> getdateslot(
            @Field("user_id") String user_id);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.PROFILE_UPDATE)
    Call<String> doProfileUpdate(
            @Field("user_id") String user_id,
            @Field("user_phone") String user_phone,
            @Field("user_first_name") String user_first_name,
            @Field("user_last_name") String user_last_name,
            @Field("user_email") String user_email,
            @Field("device_id") String device_id,
            @Field("fcm_token") String user_fcm_token);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.CHANGE_PASSWORD)
    Call<String> doChangePassword(
            @Field("user_id") String user_id,
            @Field("user_old_password") String user_old_password,
            @Field("user_new_password") String user_new_password);
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.ADDRESS_BOOK)
    Call<String> getAddress(
            @Field("ab_user_id") String user_id);
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.PRODUCT)
    Call<String> getProduct(
            @Field("category_id") String user_id);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.REMOVE_ADDRESS)
    Call<String> removeAddress(
            @Field("ab_id") String ab_id,
            @Field("ab_user_id") String user_id);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.ADD_ADDRESS)
    Call<String> doaddnewaddress(
            @Field("address_id") String address_id,
            @Field("ab_name") String ab_name,
            @Field("ab_mobile_number") String ab_mobile_number,
            @Field("ab_pincode") String ab_pincode,
            @Field("ab_state") String ab_state,
            @Field("ab_address") String ab_address,
            @Field("ab_locallity") String ab_locallity,
            @Field("ab_type") String ab_type,
            @Field("ab_user_id") String user_id);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.FIRST_REGISTRATION)
    Call<String> doFirstRegistration(
            @Field("user_phone") String user_phone);


    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.OTP_VERIFICATION)
    Call<String> otpVerification(
            @Field("user_id") String user_id,
            @Field("user_otp") String otp);

}
