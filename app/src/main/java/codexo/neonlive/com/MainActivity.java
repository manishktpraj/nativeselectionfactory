package codexo.neonlive.com;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import codexo.neonlive.Subcategory;
import codexo.neonlive.com.databinding.ActivityMainBinding;
import codexo.neonlive.com.ui.draft.DraftFragment;
import codexo.neonlive.com.ui.draft.Prefrences.SharedPre;
import codexo.neonlive.com.ui.player.IntentUtil;
import codexo.neonlive.com.ui.player.YoutubeExoCustomPlayer;
import codexo.neonlive.SessionManager;
import codexo.neonlive.com.ui.youtube.CustomYoutube;
import codexo.neonlive.com.ui.youtube.YoutubeActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {
private ActivityMainBinding binding;
    private AppBarConfiguration mAppBarConfiguration;
    AppCompatActivity activity;
    RecyclerView recycler_view;
    private ProgressDialog loading;
    private static LinearLayoutManager mLayoutManager;
    JSONArray jsonaryvideo, result_cat;
    SessionManager session;
    TextView usernamef, usernumber;
    TabItem livevideos, allvideos;
    TabLayout tablayoutdata;
    Integer position;
    LinearLayout logout, progreeslayout;
    private FragmentManager manager;
    private FragmentTransaction transaction;
    private EndlessRecyclerViewScrollListener scrollListener;
    CustomAdapter adapter;

    private FirebaseDatabase mDatabase2;
    DatabaseReference messagesRef2;
    private ValueEventListener mSearchedLocationReferenceListener2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding= DataBindingUtil.setContentView(this,R.layout.activity_main);
        activity = MainActivity.this;
        manager = getSupportFragmentManager();

        loading = Constant.getProgressBar(MainActivity.this);
        recycler_view = findViewById(R.id.recycler_view);
        progreeslayout = findViewById(R.id.progreeslayout);

        session = new SessionManager(getApplicationContext());
        usernamef = findViewById(R.id.username);
        usernumber = findViewById(R.id.usernumber);

        livevideos = findViewById(R.id.livevideos);
        allvideos = findViewById(R.id.allvideos);
        logout = findViewById(R.id.logout);

        String username = session.getFirstName() + " " + session.getLastName();
        String phone = session.getUserPhone();
        usernumber.setText(phone);
        usernamef.setText(username);

        tablayoutdata = (TabLayout) findViewById(R.id.tablayoutdata);

        tablayoutdata.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        String MESSAGE_CHANNEL = "login/" + session.getUserId();
        String device_id = Constant.device_id;

        Log.d("Locations device_id", "location: " + device_id);
        Log.d("MESSAGE_CHANNEL", "location: " + MESSAGE_CHANNEL);
        mDatabase2 = FirebaseDatabase.getInstance();
        messagesRef2 = mDatabase2.getReference().child(MESSAGE_CHANNEL);
        messagesRef2.setValue(device_id);

        mSearchedLocationReferenceListener2 = messagesRef2.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String dv = dataSnapshot.getValue().toString();

                if (dv != device_id) {
                    forcelogout();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });


        String userid = session.getUserId();
       // String userid = "1707";
        SharedPre.getInstance(this).setUserId(userid+"Selection-Factory");

        this.getdashboard(userid, 0);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Log Out!");
                builder.setMessage("Are you sure?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                session.logoutUser();
                                finish();
                            }
                        }, 000);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

                Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                nbutton.setTextColor(Color.parseColor("#D6483F"));

                Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                pbutton.setTextColor(Color.parseColor("#008000"));
            }
        });
    }

    private void forcelogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Multiple Login");
        builder.setMessage("you have login in another device.You have logout to this device.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        session.logoutUser();
                        finish();
                    }
                }, 000);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.parseColor("#008000"));

    }

    private void forceupdate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("New Update Available");
        builder.setMessage("Performance Improved");
        builder.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        session.logoutUser();
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=codexo.neonlive.com"); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                }, 000);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.parseColor("#008000"));

    }

    private void setCurrentItem(final Integer position) {
        if (position == 0) {
            binding.contentMain.main.container.setVisibility(View.GONE);
            binding.contentMain.main.recyclerView.setVisibility(View.VISIBLE);
            binding.contentMain.main.progreeslayout.setVisibility(View.GONE);
            adapter = new CustomAdapter(activity);
            mLayoutManager = new LinearLayoutManager(activity,
                    LinearLayoutManager.VERTICAL,
                    false);
            recycler_view.setLayoutManager(mLayoutManager);
            ///   recycler_view.setHasFixedSize(true);
            recycler_view.setAdapter(adapter);

            scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    // Triggered only when new data needs to be appended to the list
                    // Add whatever code is needed to append new items to the bottom of the list
                    getdashboard(session.getUserId(), page);
                }
            };
            // Adds the scroll listener to RecyclerView
            recycler_view.addOnScrollListener(scrollListener);


        } else if (position == 1) {
            binding.contentMain.main.container.setVisibility(View.GONE);
            binding.contentMain.main.recyclerView.setVisibility(View.VISIBLE);
            binding.contentMain.main.progreeslayout.setVisibility(View.GONE);
            final CustomAdapterCat adapter = new CustomAdapterCat(activity);
            mLayoutManager = new LinearLayoutManager(activity,
                    LinearLayoutManager.VERTICAL,
                    false);
            recycler_view.setLayoutManager(mLayoutManager);
            recycler_view.setHasFixedSize(true);
            recycler_view.setAdapter(adapter);

        } else {
            binding.contentMain.main.container.setVisibility(View.VISIBLE);
            binding.contentMain.main.recyclerView.setVisibility(View.GONE);
            binding.contentMain.main.progreeslayout.setVisibility(View.GONE);
            transaction = manager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.container, DraftFragment.newInstance());
            if (!isFinishing() && !isDestroyed()) {
                transaction.commit();
            }

        }
    }

    private void getdashboard(final String user_id, final int pageno) {
        if (!loading.isShowing() && recycler_view.getAdapter() == null) {
            loading.show();
            progreeslayout.setVisibility(View.GONE);
        } else {

            progreeslayout.setVisibility(View.VISIBLE);

        }
        Log.d(pageno + "", "pageno");
        Call<String> call = Constant.getUrl().getDashboardData(user_id, Constant.fcm_token, Constant.device_id, "", pageno);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {
                if (loading.isShowing()) {
                    loading.dismiss();
                } else {
                    progreeslayout.setVisibility(View.GONE);

                }
                String response = response_string.body();

                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String version = jsonObject.getString("version");
                        String versionCode = BuildConfig.VERSION_NAME;
                        // if (message.equals("ok") && version.equals(versionCode)) {
                        if (message.equals("ok")) {
                            loading.dismiss();
                            ////// Log.d(Constant.device_id,"device_id");
                            Log.d(Constant.fcm_token + "empty", "device_idt");

                            result_cat = jsonObject.getJSONArray("result_cat");
                           /* final CustomAdapter adapter = new CustomAdapter(activity);
                            mLayoutManager = new LinearLayoutManager(activity,
                                    LinearLayoutManager.VERTICAL,
                                    false);
                            recycler_view.setLayoutManager(mLayoutManager);
                            recycler_view.setHasFixedSize(true);
                            recycler_view.setAdapter(adapter);*/
                            if (recycler_view.getAdapter() == null) {
                                jsonaryvideo = jsonObject.getJSONArray("result_video");
                                setCurrentItem(0);
                            } else {
                                JSONArray jv = jsonObject.getJSONArray("result_video");
                                for (int i = 0; i < jv.length(); i++) {
                                    jsonaryvideo.put(jv.get(i));
                                }
                                /// jsonaryvideo = jsonObject.getJSONArray("result_video");
                                adapter.notifyDataSetChanged(); // or notifyItemRangeRemoved
                                /// scrollListener.resetState();

                            }

                        } else if (message.equals("ok") && !version.equals(versionCode)) {
                            forceupdate();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getdashboard(user_id, pageno);
            }
        });

    }


    public class CustomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_video_list, parent, false);
            MainListHolder listHolder = new MainListHolder(itemView);
            return listHolder;

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final MainListHolder mainHolder = (MainListHolder) holder;


            try {
                mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //  Intent i = new Intent(activity, YoutubeActivity.class);


                        JSONObject childata = null;
                        try {
                            childata = jsonaryvideo.getJSONObject(position);

                            if (childata.getInt("vid_type") == 1) {
                                Intent i = new Intent(activity, YoutubeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.putExtra("videodetail", childata + "");
                                try {
                                    i.putExtra("id", childata.getString("vid_id"));
                                    i.putExtra("title", childata.getString("vid_title"));
                                    i.putExtra("video_id", childata.getString("vid_url"));
                                    i.putExtra("dis", childata.getString("vid_desc"));
                                    i.putExtra("image", jsonaryvideo.getJSONObject(position).getString("vid_image"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                activity.startActivity(i);
                            } else {
                                //Intent i = new Intent(activity, YoutubeExoCustomPlayer.class);
                                Intent i = new Intent(activity, YoutubeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.putExtra("videodetail", childata + "");
                                try {
                                    i.putExtra("id", childata.getString("vid_id"));
                                    i.putExtra("title", childata.getString("vid_title"));
                                    i.putExtra("video_id", childata.getString("vid_url"));
                                    i.putExtra("offline", childata.getString("vid_offline_url"));
                                    i.putExtra("dis", childata.getString("vid_desc"));
                                    i.putExtra("image", jsonaryvideo.getJSONObject(position).getString("vid_image"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                activity.startActivity(i);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
                mainHolder.title.setText(jsonaryvideo.getJSONObject(position).getString("vid_title"));

                String imageUri = jsonaryvideo.getJSONObject(position).getString("vid_image");
                Log.d(imageUri, "  onBindViewHolder: ");
                if (!imageUri.equals("")) {
                    Constant.setImage(imageUri, mainHolder.video_image);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonaryvideo.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top, title;
            ImageView video_image;
            CardView card;

            public MainListHolder(View view) {
                super(view);
                title = view.findViewById(R.id.title);
                video_image = view.findViewById(R.id.video_image_);
                card = view.findViewById(R.id.card);

            }
        }
    }

    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_cat_list, parent, false);
            MainListHolder listHolder = new MainListHolder(itemView);
            return listHolder;

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final MainListHolder mainHolder = (MainListHolder) holder;


            try {
                mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //  Intent i = new Intent(activity, YoutubeActivity.class);
                        Intent i = new Intent(activity, Subcategory.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        JSONObject childata = null;
                        String cat_name = "", cat_id = "";
                        try {
                            childata = result_cat.getJSONObject(position);
                            cat_name = childata.getString("bcat_name");
                            cat_id = childata.getString("bcat_id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        i.putExtra("cat_name", cat_name);
                        i.putExtra("cat_id", cat_id);
                        activity.startActivity(i);
                    }
                });
                mainHolder.title.setText(result_cat.getJSONObject(position).getString("bcat_name"));

                String imageUri = result_cat.getJSONObject(position).getString("bcat_image");
                Log.d(imageUri, "  onBindViewHolder: ");
                if (!imageUri.equals("")) {
                    Constant.setImage(imageUri, mainHolder.video_image);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            if (result_cat != null) {
                return result_cat.length();
            } else {
                return 0;
            }

        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top, title;
            ImageView video_image;
            CardView card;

            public MainListHolder(View view) {
                super(view);
                title = view.findViewById(R.id.title);
                video_image = view.findViewById(R.id.video_image_);
                card = view.findViewById(R.id.card);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Log Out!");
                builder.setMessage("Are you sure?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                session.logoutUser();
                                finish();
                            }
                        }, 000);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

                Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                nbutton.setTextColor(Color.parseColor("#D6483F"));

                Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                pbutton.setTextColor(Color.parseColor("#008000"));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}