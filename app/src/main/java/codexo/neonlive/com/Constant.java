package codexo.neonlive.com;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import codexo.neonlive.com.GetResponse;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Constant {

    public static final String YOUR_NEW_API_URL_LIVE  = "https://neonclasses.com/webapi/neonlive/";
    public static final String NO_IMAGE_URL  = "https://via.placeholder.com/150/f5f5f5/000%20?Text=neonclasses.com";

    public static final String CHECK_REGISTER = "register-with-site-webservice.php?";
    public static final String OTP_VERIFY_FOR_REGISTER = "register-with-site-webservice.php?";
    public static final String REGISTER = "register/finalregis";
    public static final String LOGIN = "loginuser";
    public static final String SLIDER = "register/slider";
    public static final String PROFILE_UPDATE = "register/updateuserprofile";
    public static final String CHANGE_PASSWORD = "register/changepassword";
    public static final String FIRST_REGISTRATION = "register/index";
    public static final String OTP_VERIFICATION = "verifyotp";
    public static final String ADDRESS_BOOK= "addressbook";
    public static final String PRODUCT= "product";
    public static final String OFFERCE= "dashboard/offerce";
    public static final String DATESLOT= "dashboard/deliverytimepicktime";

    public static final String REMOVE_ADDRESS= "addressbook/removeaddress";
    public static final String DEFAULT_ADDRESS= "addressbook/setDefaultAddress";
    public static final String ADD_ADDRESS= "addressbook/addaddress";

    public static final String FAQ = "faq/index";
    public static final String DASHBOARD = "getdashboard";
    public static final String SUBCATEGORY = "getsubcategory";


    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Neeonclasses Coding Notification";
    public static final String CHANNEL_DESCRIPTION = "www.neonclasses.com";

    public static String token = "";
    public static String fcm_call = "";

    public static String category_url = "";
    public static String product_url = "";
    public static String address_from = "";

    public static TextView user_name;
    public static String fragments = "home";
    public static String user_id = "0";
    public static String user_phone = "0";

    public static String from_checkout = "No";
    public static String cart_category_type = "0";
    public static String address_status = "0";

    public static JSONArray category_array = new JSONArray();
    public static JSONArray product_array = new JSONArray();
    public static JSONArray offers_array = new JSONArray();
    public static JSONArray cart_array = new JSONArray();
    public static JSONObject cart_estimation_object = new JSONObject();
    public static JSONObject address_result = new JSONObject();
    public static JSONArray schedule_date_array = new JSONArray();
    public static JSONArray schedule_time_array = new JSONArray();
    public static JSONArray schedule_ddate_array = new JSONArray();


    public static ArrayList<String> snack_array = new ArrayList<>();

    public static GetResponse getUrl() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(1000, TimeUnit.MILLISECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(codexo.neonlive.com.Constant.YOUR_NEW_API_URL_LIVE)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        return retrofit.create(GetResponse.class);
    }

    public static boolean isNetworkConnectionAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (isConnected) {
            Log.d("Network", "Connected to internet");
            return true;
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("No internet Connection");
            builder.setMessage("Please turn on internet connection to continue");
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            return false;
        }
    }



    public static void fullScreen(AppCompatActivity activity) {
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = activity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }


    public static void callBack(final AppCompatActivity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        activity.finishAffinity();
                        System.exit(0);
                    }
                }, 000);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

        Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.parseColor("#D6483F"));

        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.parseColor("#008000"));

    }

    public static void logPrint(String title, String message) {

        Log.d(title, message + "");

    }

    public static void hideKeyBoard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @SuppressLint("RestrictedApi")
    public static void setToolbar(final AppCompatActivity activity, Toolbar toolbar) {
        activity.setSupportActionBar(toolbar);
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onBackPressed();
                }
            });
            toolbar.getNavigationIcon().setColorFilter(activity.getResources().getColor(R.color.back), PorterDuff.Mode.SRC_ATOP);
            activity.setTitle("");

        }
    }

    public static void printHashKey(AppCompatActivity activity) {
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("TAG", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("TAG", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("TAG", "printHashKey()", e);
        }
    }

    public static ColorStateList getColorState() {

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{

                        Color.parseColor("#666666") //disabled
                        , Color.parseColor("#fdbc0a") //enabled

                }
        );
        return colorStateList;
    }

    public static ProgressDialog getProgressBar(Context context) {
        ProgressDialog loading = new ProgressDialog(context);
        loading.setMessage("Please wait... For a while");
        loading.setIndeterminate(true);
        loading.setCancelable(false);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.layout_process_bar));
        return loading;
    }

    public static Typeface getFonts(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public static Typeface getFontsBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
    }

    public static Typeface getFontsExtraBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-ExtraBold.ttf");
    }

    public static String fcm_token = "";

    public void writeTokenToFile(AppCompatActivity activity, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(activity.openFileOutput("token.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String readTokenFromFile(AppCompatActivity activity) {
        String ret = "";
        try {
            InputStream inputStream = activity.openFileInput("token.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

    public static ImageLoader imageLoader_;

    public static ImageLoaderConfiguration getImageLoaderConfig(Context context) {
        DisplayImageOptions options_dis = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_img)
                .showImageForEmptyUri(R.drawable.no_img)
                .showImageOnFail(R.drawable.no_img)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        return new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options_dis)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
    }

    private static DisplayImageOptions getImageOptions() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_img)
                .showImageForEmptyUri(R.drawable.no_img)
                .showImageOnFail(R.drawable.no_img)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
    public static void setImage(String url, final ImageView imageView) {

        if (url.equals("")) {
            imageView.setImageResource(R.drawable.no_img);
        } else {

            imageLoader_.displayImage(url, imageView, Constant.getImageOptions(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            imageView.setImageResource(R.drawable.no_img);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            imageView.setImageResource(R.drawable.no_img);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            imageView.setImageBitmap(loadedImage);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            imageView.setImageResource(R.drawable.no_img);
                        }
                    }
            );
        }

    }

    public static final int MULTIPLE_PERMISSIONS = 100;

    public static boolean checkPermissions(AppCompatActivity activity, String[] permissions) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static String login_from = "";
    public static String skip = "";
    public static String device_id = "";

    public static void writeSkipToFile(AppCompatActivity activity, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(activity.openFileOutput("skip.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String readSkipFromFile(AppCompatActivity activity) {
        String ret = "no";
        try {
            InputStream inputStream = activity.openFileInput("skip.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }



    public static double cartCalculation() {

        double item_total_price = 0.00;

        try{
            for (int i = 0; i< codexo.neonlive.com.Constant.cart_array.length(); i++){

                String product_original_price = codexo.neonlive.com.Constant.cart_array.optJSONObject(i).getString("product_net_price").trim();

                String qty = codexo.neonlive.com.Constant.cart_array.getJSONObject(i).getString("qty");

                int qty_ = Integer.parseInt(qty);

                item_total_price = item_total_price+ Double.parseDouble(product_original_price) * qty_;


            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return item_total_price;
    }
}
