package codexo.neonlive.com;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import codexo.Login;
import codexo.neonlive.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;

public class Otp extends AppCompatActivity {
    AppCompatActivity activity;
    TextInputEditText mobile_number;
    Button login;
    private ProgressDialog loading;
    SessionManager session;
    String user_id="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp2);

        activity = Otp.this;
        login =findViewById(R.id.login);
        mobile_number  = findViewById(R.id.mobile_number);
        loading = Constant.getProgressBar(activity);
        session = new SessionManager(getApplicationContext());
        user_id = getIntent().getStringExtra("id");
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String go_next = "yes";
                String username_string = mobile_number.getText().toString();
                if(username_string.length()<4 || username_string.length()>4)
                {
                    int duration = Toast.LENGTH_SHORT;
                    mobile_number.setError("Please Enter a Valid 4 digit OTP");
                    go_next ="no";
                }

                if(go_next.equals("yes")) {
                    doLogin(user_id,username_string);
                  /*  Intent i = new Intent(activity, Otp.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(i);*/
                }
            }
        });
    }



    private void doLogin(final String username_string, final String password_string) {

        if (!loading.isShowing()){
            loading.show();
        }
        Call<String> call = Constant.getUrl().otpVerification(username_string,password_string);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                loading.dismiss();

                String response = response_string.body();

                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                          //  Constant.skip="yes";
                          ///  Constant.fragments="main";

                            Toast.makeText(activity, "Login successfully", Toast.LENGTH_SHORT).show();

                            JSONObject localJSONObject2 = jsonObject.getJSONObject("results");
                              session.createLoginSession(localJSONObject2);

                               Intent i = new Intent(activity, MainActivity.class);
                          i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                           i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            activity.startActivity(i);
                      finish();
                        } else {
                            new AlertDialog.Builder(activity)
                                    .setCancelable(false)
                                    .setMessage("OTP does not match.")
                                    .setNegativeButton("ok", null).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                doLogin(username_string, password_string);
            }
        });
    }
}